#include <nbtpp2/nbt_file.hpp>
#include <nbtpp2/all_tags.hpp>

void dump_tag(nbtpp2::Tag *tag, int ntab);

void print_tabs(int ntab)
{
    while (ntab-- > 0) {
        std::cout << "\t";
    }
}

void dump_tag_end()
{
    std::cout << "TAG_End";
}

void dump_tag_byte(std::int8_t value)
{
    std::cout << "TAG_Byte { "
              << std::hex
              << "0x"
              << static_cast<int>(value)
              << std::dec
              << " }";
}

void dump_tag_short(std::int16_t value)
{
    std::cout << "TAG_Short { " << static_cast<int>(value) << " }";
}

void dump_tag_int(std::int32_t value)
{
    std::cout << "TAG_Int { " << value << " }";
}

void dump_tag_long(std::int64_t value)
{
    std::cout << "TAG_Long { " << value << " }";
}

void dump_tag_float(float value)
{
    std::cout << "TAG_Float { " << value << " }";
}

void dump_tag_double(double value)
{
    std::cout << "TAG_Double { " << value << " } ";
}

void dump_tag_byte_array(std::vector<std::int8_t> &value)
{
    auto sep = std::string{};
    std::cout << "TAG_Byte_Array { ";
    for (auto el : value) {
        std::cout << sep
                  << std::hex
                  << "0x"
                  << static_cast<int>(el)
                  << std::dec;
        sep = ", ";
    }
    std::cout << " }";
}

void dump_tag_string(std::string &value)
{
    std::cout << "TAG_String { \"" << value << "\" }";
}

void dump_tag_list(std::vector<nbtpp2::Tag *> &tl, int ntab)
{
    std::cout << "TAG_List {";
    if (!tl.empty()) {
        std::cout << "\n";
        for (auto it = tl.begin(); it < tl.end(); ++it) {
            print_tabs(ntab + 1);
            std::cout << it - tl.begin() << ": ";
            dump_tag(*it, ntab + 1);
            std::cout << ",\n";
        }
        print_tabs(ntab);
    }
    std::cout << "}";
}

void dump_tag_compound(std::map<std::string, nbtpp2::Tag *> &tc, int ntab)
{
    std::cout << "TAG_Compound {";
    if (!tc.empty())
    {
        std::cout << "\n";
        for (auto &tag_pair : tc) {
            print_tabs(ntab + 1);
            std::cout << '\"' << tag_pair.first << '\"' << ": ";
            dump_tag(tag_pair.second, ntab + 1);
            std::cout << ",\n";
        }
        print_tabs(ntab);
    }
    std::cout << "}";
}

void dump_tag_int_array(std::vector<std::int32_t> &value)
{
    auto sep = std::string{};
    std::cout << "TAG_Int_Array { ";
    for (auto el : value) {
        std::cout << sep << static_cast<int>(el);
        sep = ", ";
    }
    std::cout << " }";
}

void dump_tag_long_array(std::vector<std::int64_t> &value)
{
    auto sep = std::string{};
    std::cout << "TAG_Long_Array { ";
    for (auto el : value) {
        std::cout << sep << static_cast<int>(el);
        sep = ", ";
    }
    std::cout << " }";
}

void dump_tag(nbtpp2::Tag *tag, int ntab)
{
    switch (tag->identify()) {
    case nbtpp2::TagType::TagEnd: dump_tag_end();
        break;
    case nbtpp2::TagType::TagByte: dump_tag_byte(tag->as<nbtpp2::tags::TagByte>().value);
        break;
    case nbtpp2::TagType::TagShort: dump_tag_short(tag->as<nbtpp2::tags::TagShort>().value);
        break;
    case nbtpp2::TagType::TagInt: dump_tag_int(tag->as<nbtpp2::tags::TagInt>().value);
        break;
    case nbtpp2::TagType::TagLong: dump_tag_long(tag->as<nbtpp2::tags::TagLong>().value);
        break;
    case nbtpp2::TagType::TagFloat: dump_tag_float(tag->as<nbtpp2::tags::TagFloat>().value);
        break;
    case nbtpp2::TagType::TagDouble: dump_tag_double(tag->as<nbtpp2::tags::TagDouble>().value);
        break;
    case nbtpp2::TagType::TagByteArray: dump_tag_byte_array(tag->as<nbtpp2::tags::TagByteArray>().value);
        break;
    case nbtpp2::TagType::TagString: dump_tag_string(tag->as<nbtpp2::tags::TagString>().value);
        break;
    case nbtpp2::TagType::TagList: dump_tag_list(tag->as<nbtpp2::tags::TagList>().value, ntab);
        break;
    case nbtpp2::TagType::TagCompound: dump_tag_compound(tag->as<nbtpp2::tags::TagCompound>().value, ntab);
        break;
    case nbtpp2::TagType::TagIntArray: dump_tag_int_array(tag->as<nbtpp2::tags::TagIntArray>().value);
        break;
    case nbtpp2::TagType::TagLongArray: dump_tag_long_array(tag->as<nbtpp2::tags::TagLongArray>().value);
        break;
    }
}

int main(int argc, char *argv[])
{
    if (argc != 2) {
        std::cerr << "Error: one argument expected\n";
        std::cout << "Usage: " << argv[0] << " <file>\n";
        return 1;
    }

    auto file = nbtpp2::NbtFile(argv[1], nbtpp2::Endianness::Big);
    std::cout << '\"' << file.root_name << '\"' << ": ";
    dump_tag(&file.get_root_tag(), 0);
    std::cout << "\n";

    return 0;
}
